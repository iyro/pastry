README

Pastry

Compile :

To compile use ant (https://ant.apache.org/bindownload.cgi)

The project build xml is build/build.xml

cd build
ant

Run :

The jar files will be created in out/artifacts

java -jar <jarfile>

To simplify deployment I have included a script launchScript.sh which :
1. Compiles the source code.
2. Generates a jar file.
3. Deploys the jar file to my home directory on the CS120 machines.
4. Starts the discovery node on albany.
5. Starts N Peers at other hosts based on the configuration in the script.
6. Optionally the KEYS file can be populated with custom identifiers, random identifiers are generated if a blank line or EOF is encountered.
