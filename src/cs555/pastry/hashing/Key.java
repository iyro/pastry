package cs555.pastry.hashing;

import cs555.pastry.exceptions.IncorrectKeySizeException;
import cs555.pastry.utilities.LoggingUtility;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Random;
import java.util.logging.Logger;

import static cs555.pastry.utilities.Constants.*;

/**
 * Created by iyro on 9/29/15.
 */
public class Key implements Serializable, Comparable<Key> {
    private static final Logger LOGGER = LoggingUtility.getLogger(Key.class.getName());
    private static final long serialVersionUID = 6101173971785453574L;
    private byte[] key;

    //Key is provided by end-user.
    public Key(Type type, String key) throws IncorrectKeySizeException {
        if (type == Type.KEY) {
            this.key = new byte[KEY_SIZE_IN_BYTES];
            int size = key.length();

            if (size == KEY_SIZE_IN_HEX_DIGITS) {
                int j = 0;
                for (int i = 0; i < size; i++) {
                    String a = key.substring(i, i + 2);
                    int valA = Integer.parseInt(a, 16);
                    i++;
                    this.key[j] = (byte) valA;
                    j++;
                }
            } else {
                throw new IncorrectKeySizeException();
            }
        } else {
            this.key = new byte[KEY_SIZE_IN_BYTES];
            int intKey = Arrays.hashCode(key.getBytes()) % IDENTIFIER_SPACE_SIZE;
            this.key[0] = (byte) (intKey & 0xff);
            this.key[1] = (byte) ((intKey >> 8) & 0xff);
        }
    }

    //Random key generation using timestamp.
    public Key() {
        this.key = new byte[KEY_SIZE_IN_BYTES];
        Random rn = new Random(System.currentTimeMillis());
        rn.nextBytes(this.key);
    }

    //Key generation based on Data
    public Key(byte[] data) {
        short shortKey = ((short) (Arrays.hashCode(data) % IDENTIFIER_SPACE_SIZE));
        this.key[0] = (byte) (shortKey & 0xff);
        this.key[1] = (byte) ((shortKey >> 8) & 0xff);
    }

    public static int distanceBetween(Key newKey, Key originalKey) {
        int newKeyNumeric = newKey.getNumericKey();
        int originalKeyNumeric = originalKey.getNumericKey();

        if (Math.abs(originalKeyNumeric - newKeyNumeric) == 0) {
            return IDENTIFIER_SPACE_SIZE;
        } else if (Math.abs(originalKeyNumeric - newKeyNumeric) < (IDENTIFIER_SPACE_SIZE / 2)) {
            return Math.abs(originalKeyNumeric - newKeyNumeric);
        } else {
            return IDENTIFIER_SPACE_SIZE - Math.abs(originalKeyNumeric - newKeyNumeric);
        }
    }

    //For printing keys
    public String toString() {
        StringBuffer strBuf = new StringBuffer();
        for (int i = 0; i < this.key.length; i++) {
            int byteValue = (int) this.key[i] & 0xff;
            if (byteValue <= 15) {
                strBuf.append("0");
            }
            strBuf.append(Integer.toString(byteValue, 16));
        }
        return strBuf.toString();
    }

    public byte[] getKey() {
        return key;
    }

    public Integer getNumericKey() {
        return Integer.parseInt(toString(), 16);
    }

    @Override
    public int hashCode() {
        return Arrays.hashCode(this.key);
    }

    @Override
    public boolean equals(Object other) {
        if (other instanceof Key) {

            if (other == this) {
                return true;
            }

            if (other == null) {
                return false;
            }

            Key otherKey = (Key) other;
            return Arrays.equals(this.key, otherKey.getKey());
        } else {
            return false;
        }
    }

    @Override
    public int compareTo(Key key) {
        return getNumericKey().compareTo(key.getNumericKey());
    }

    public enum Type {
        FILE_NAME, KEY
    }
}
