package cs555.pastry.items;

import cs555.pastry.utilities.LoggingUtility;

import java.io.*;
import java.util.logging.Logger;

import static cs555.pastry.utilities.Constants.CHUNK_SIZE;

/**
 * Created by iyro on 9/25/15.
 */
public class DataChunk implements Serializable {
    private static final Logger LOGGER = LoggingUtility.getLogger(DataChunk.class.getName());
    private static final long serialVersionUID = -2018463952634120008L;
    private long chunkID;
    private String name;
    private byte[] data;
    private long fileSize;

    public DataChunk(String name, long fileSize, long chunkID) {
        this.name = name;
        this.chunkID = chunkID;
        this.fileSize = fileSize;
    }

    public boolean read() throws IOException {
        FileInputStream file = new FileInputStream(name);
        if (file.getFD().valid()) {
            long totalChunks;

            totalChunks = fileSize / CHUNK_SIZE;

            if (fileSize % CHUNK_SIZE != 0) {
                totalChunks++;
            }

            if (chunkID == totalChunks - 1) {
                this.data = new byte[(int) (fileSize - (chunkID * CHUNK_SIZE))];
            } else {
                this.data = new byte[(int) CHUNK_SIZE];
            }

            long skippedBytes = file.skip(chunkID * CHUNK_SIZE);

            if (skippedBytes == chunkID * CHUNK_SIZE) {
                int bytesRead = file.read(this.data);
                if (bytesRead == this.data.length) {
                    file.close();
                    return true;
                } else {
                    file.close();
                    this.data = null;
                    return false;
                }
            } else {
                file.close();
                this.data = null;
                return false;
            }
        } else {
            this.data = null;
            return false;
        }
    }

    public boolean writeDHT() throws IOException {
        File fd = new File("/tmp/iyro_Pastry/" + name);
        if (this.data != null) {
            if (!fd.getParentFile().exists()) {
                fd.getParentFile().mkdirs();
            }
            RandomAccessFile file = new RandomAccessFile("/tmp/iyro_Pastry/" + name, "rws");
            file.seek(chunkID * CHUNK_SIZE);
            file.write(this.data);
            file.close();
            this.data = null;
            return true;
        } else {
            LOGGER.severe("Data is null.");
            return false;
        }
    }

    public boolean readDHT() throws IOException {
        FileInputStream file = new FileInputStream("/tmp/iyro_Pastry/" + name);
        if (file.getFD().valid()) {
            long totalChunks;

            totalChunks = fileSize / CHUNK_SIZE;

            if (fileSize % CHUNK_SIZE != 0) {
                totalChunks++;
            }

            if (chunkID == totalChunks - 1) {
                this.data = new byte[(int) (fileSize - (chunkID * CHUNK_SIZE))];
            } else {
                this.data = new byte[(int) CHUNK_SIZE];
            }

            long skippedBytes = file.skip(chunkID * CHUNK_SIZE);

            if (skippedBytes == chunkID * CHUNK_SIZE) {
                int bytesRead = file.read(this.data);
                if (bytesRead == this.data.length) {
                    file.close();
                    return true;
                } else {
                    file.close();
                    this.data = null;
                    return false;
                }
            } else {
                file.close();
                this.data = null;
                return false;
            }
        } else {
            this.data = null;
            return false;
        }

    }

    public boolean write() throws IOException {
        File fd = new File(name);

        if (this.data != null) {
            if (!fd.getParentFile().exists()) {
                fd.getParentFile().mkdirs();
            }
            RandomAccessFile file = new RandomAccessFile(name, "rws");
            file.seek(chunkID * CHUNK_SIZE);
            file.write(this.data);
            file.close();
            this.data = null;
            return true;
        } else {
            LOGGER.severe("Data is null.");
            return false;
        }
    }

    public String getDHTName() {
        return ("/tmp/iyro_Pastry/" + name);
    }

    public String getName() {
        return name;
    }

    public long getFileSize() {
        return fileSize;
    }

    public byte[] getData() {
        return data;
    }
}
