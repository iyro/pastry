package cs555.pastry.items;

import cs555.pastry.address.NodeInformation;
import cs555.pastry.hashing.Key;
import cs555.pastry.utilities.LoggingUtility;

import java.io.Serializable;
import java.util.logging.Logger;

/**
 * Created by iyro on 10/13/15.
 */
public class DataItem implements Serializable {
    private static final Logger LOGGER = LoggingUtility.getLogger(DataItem.class.getName());
    private static final long serialVersionUID = 796268774436417030L;
    private Key key;
    private NodeInformation nodeInformation;
    private String name;
    private long fileSize;

    public DataItem(Key key, NodeInformation nodeInformation, String name, long fileSize) {
        this.key = key;
        this.nodeInformation = nodeInformation;
        this.name = name;
        this.fileSize = fileSize;
    }

    public Key getKey() {
        return key;
    }

    public NodeInformation getNodeInformation() {
        return nodeInformation;
    }

    public void setNodeInformation(NodeInformation nodeInformation) {
        this.nodeInformation = nodeInformation;
    }

    public String getName() {
        return name;
    }

    public long getFileSize() {
        return fileSize;
    }

    public String getDHTName() {
        return ("/tmp/iyro_Pastry/" + name);
    }

    public String toString() {
        StringBuilder result = new StringBuilder("Key : " + key.toString() + ", Name : " + getName() + ", Size : " + getFileSize());
        return result.toString();
    }
}
