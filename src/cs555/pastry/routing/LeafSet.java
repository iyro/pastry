package cs555.pastry.routing;

import cs555.pastry.address.NodeInformation;
import cs555.pastry.exceptions.ReceivedMyOwnKeyException;
import cs555.pastry.hashing.Key;
import cs555.pastry.packets.JoinResponse;
import cs555.pastry.packets.RoutingUpdate;
import cs555.pastry.utilities.LoggingUtility;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.logging.Logger;

import static cs555.pastry.utilities.Constants.LEAF_SET_SIZE;

/**
 * Created by iyro on 10/5/15.
 */
public class LeafSet implements Serializable {
    private static final Logger LOGGER = LoggingUtility.getLogger(LeafSet.class.getName());
    private static final long serialVersionUID = -8590229974554135608L;
    private NodeInformation nodeInformation;
    private NodeInformation[] leafSetNeighbors;

    public LeafSet(NodeInformation nodeInformation) {
        this.nodeInformation = nodeInformation;
        this.leafSetNeighbors = new NodeInformation[LEAF_SET_SIZE];
        Arrays.fill(this.leafSetNeighbors, nodeInformation);
    }

    public NodeInformation getLeftNeighbor() {
        return leafSetNeighbors[0];
    }

    public void setLeftNeighbor(NodeInformation leafSetNeighbor) {
        this.leafSetNeighbors[0] = leafSetNeighbor;
    }

    public NodeInformation getRightNeighbor() {
        return leafSetNeighbors[1];
    }

    public void setRightNeighbor(NodeInformation leafSetNeighbor) {
        this.leafSetNeighbors[1] = leafSetNeighbor;
    }

    public NodeInformation getNodeInformation() {
        return nodeInformation;
    }

    public NodeInformation[] getLeafSet() {
        return leafSetNeighbors;
    }

    public NodeInformation lookup(Key key) throws IOException, ReceivedMyOwnKeyException {
        NodeInformation dispatchNode = null;

        ArrayList<Key> sortList = new ArrayList<>();
        sortList.add(nodeInformation.getKey());
        sortList.add(key);

        if (!sortList.contains(getLeftNeighbor().getKey())) {
            sortList.add(getLeftNeighbor().getKey());
        }

        if (!sortList.contains(getRightNeighbor().getKey())) {
            sortList.add(getRightNeighbor().getKey());
        }

        Collections.sort(sortList);

        int keyIndex = sortList.indexOf(key);
        int myKeyIndex = sortList.indexOf(nodeInformation.getKey());


        int keyToLeft = Key.distanceBetween(getLeftNeighbor().getKey(), key);
        int keyToRight = Key.distanceBetween(getRightNeighbor().getKey(), key);
        int keyToMe = Key.distanceBetween(nodeInformation.getKey(), key);

        if (myKeyIndex == 0) {
            if (keyIndex == sortList.size() - 1) {
                if (keyToLeft < keyToMe) {
                    dispatchNode = getLeftNeighbor();
                } else {
                    dispatchNode = nodeInformation;
                }
            }
        } else if (myKeyIndex == sortList.size() - 1) {
            if (keyIndex == 0) {
                if (keyToRight < keyToMe) {
                    dispatchNode = getRightNeighbor();
                } else {
                    dispatchNode = nodeInformation;
                }
            }
        }

        if (dispatchNode == null) {
            if (myKeyIndex + 1 == keyIndex) {
                if (keyToRight < keyToMe) {
                    dispatchNode = getRightNeighbor();
                } else {
                    dispatchNode = nodeInformation;
                }
            } else if (myKeyIndex - 1 == keyIndex) {
                if (keyToLeft < keyToMe) {
                    dispatchNode = getLeftNeighbor();
                } else {
                    dispatchNode = nodeInformation;
                }
            }
        }

        return dispatchNode;
    }

    public NodeInformation lookupExcludingSelf(Key key) {
        int distanceToLeft = Key.distanceBetween(getLeftNeighbor().getKey(), key);
        int distanceToRight = Key.distanceBetween(getRightNeighbor().getKey(), key);

        if (distanceToLeft < distanceToRight) {
            return getLeftNeighbor();
        } else {
            return getRightNeighbor();
        }
    }

    @Override
    public String toString() {
        String NEW_LINE = System.getProperty("line.separator");
        StringBuilder result = new StringBuilder("Leaf Set @ " + nodeInformation.getKey().toString());
        result.append(NEW_LINE);
        result.append("----------------");
        result.append(NEW_LINE);
        result.append("|").append(getLeftNeighbor().getKey().toString()).append("|").append(nodeInformation.getKey().toString()).append("|").append(getRightNeighbor().getKey().toString()).append("|");
        result.append(NEW_LINE);
        result.append("----------------");
        return result.toString();

    }

    public void updateLeafSet(JoinResponse joinResponse) {
            ArrayList<NodeInformation> sortList = new ArrayList<>();
            sortList.add(nodeInformation);
            sortList.add(joinResponse.getNodeInformation());

        if (joinResponse.getLeafSet() != null) {
            for (NodeInformation n : joinResponse.getLeafSet()) {
                if (n != null && !sortList.contains(n)) {
                    sortList.add(n);
                }
            }
        }

        for (NodeInformation n : getLeafSet()) {
            if (n != null && !sortList.contains(n)) {
                sortList.add(n);
            }
        }

        Collections.sort(sortList);

        int myLocation = sortList.indexOf(nodeInformation);

        if (sortList.size() == 1) {
            setLeftNeighbor(sortList.get(0));
            setRightNeighbor(sortList.get(0));
        } else if (myLocation == 0) {
            setLeftNeighbor(sortList.get(sortList.size() - 1));
            setRightNeighbor(sortList.get(myLocation + 1));
        } else if (myLocation == sortList.size() - 1) {
            setLeftNeighbor(sortList.get(myLocation - 1));
            setRightNeighbor(sortList.get(0));
        } else {
            setLeftNeighbor(sortList.get(myLocation - 1));
            setRightNeighbor(sortList.get(myLocation + 1));
        }
    }

    public void updateLeafSet(RoutingUpdate routingUpdate) {
        ArrayList<NodeInformation> sortList = new ArrayList<>();
        sortList.add(nodeInformation);
        sortList.add(routingUpdate.getNodeInformation());

        if (routingUpdate.getLeafSet() != null) {
            for (NodeInformation n : routingUpdate.getLeafSet()) {
                if (!sortList.contains(n)) {
                    sortList.add(n);
                }
            }
        }

        for (NodeInformation n : getLeafSet()) {
            if (!sortList.contains(n)) {
                sortList.add(n);
            }
        }

        Collections.sort(sortList);

        int myLocation = sortList.indexOf(nodeInformation);

        if (sortList.size() == 1) {
            setLeftNeighbor(sortList.get(0));
            setRightNeighbor(sortList.get(0));
        } else if (myLocation == 0) {
            setLeftNeighbor(sortList.get(sortList.size() - 1));
            setRightNeighbor(sortList.get(myLocation + 1));
        } else if (myLocation == sortList.size() - 1) {
            setLeftNeighbor(sortList.get(myLocation - 1));
            setRightNeighbor(sortList.get(0));
        } else {
            setLeftNeighbor(sortList.get(myLocation - 1));
            setRightNeighbor(sortList.get(myLocation + 1));
        }
    }

    public void updateLeafSetDelete(RoutingUpdate routingUpdate) {

        ArrayList<NodeInformation> sortList = new ArrayList<>();
        sortList.add(nodeInformation);

        if (routingUpdate.getLeafSet() != null) {
            for (NodeInformation n : routingUpdate.getLeafSet()) {
                if (!n.equals(routingUpdate.getNodeInformation()) && !sortList.contains(n)) {
                    sortList.add(n);
                }
            }
        }

        for (NodeInformation n : getLeafSet()) {
            if (!n.equals(routingUpdate.getNodeInformation()) && !sortList.contains(n)) {
                sortList.add(n);
            }
        }

        Collections.sort(sortList);

        int myLocation = sortList.indexOf(nodeInformation);

        if (sortList.size() == 1) {
            setLeftNeighbor(sortList.get(0));
            setRightNeighbor(sortList.get(0));
        } else if (myLocation == 0) {
            setLeftNeighbor(sortList.get(sortList.size() - 1));
            setRightNeighbor(sortList.get(myLocation + 1));
        } else if (myLocation == sortList.size() - 1) {
            setLeftNeighbor(sortList.get(myLocation - 1));
            setRightNeighbor(sortList.get(0));
        } else {
            setLeftNeighbor(sortList.get(myLocation - 1));
            setRightNeighbor(sortList.get(myLocation + 1));
        }
    }

    public void deleteLeafSetEntry(NodeInformation nodeInformation) {
        if (nodeInformation.equals(getLeftNeighbor())) {
            setLeftNeighbor(this.nodeInformation);
        }

        if (nodeInformation.equals(getRightNeighbor())) {
            setRightNeighbor(this.nodeInformation);
        }
    }
}
