package cs555.pastry.routing;

import cs555.pastry.address.NodeInformation;
import cs555.pastry.hashing.Key;
import cs555.pastry.packets.JoinResponse;
import cs555.pastry.packets.RoutingUpdate;
import cs555.pastry.utilities.LoggingUtility;

import java.io.Serializable;
import java.util.Arrays;
import java.util.logging.Logger;

import static cs555.pastry.utilities.Constants.KEY_SIZE_IN_HEX_DIGITS;
import static cs555.pastry.utilities.Constants.POSSIBLE_DIGITS_IN_HEXADECIMAL;

public class RoutingTable implements Serializable {
    private static final Logger LOGGER = LoggingUtility.getLogger(RoutingTable.class.getName());
    private static final long serialVersionUID = -5592267236315187671L;
    private NodeInformation nodeInformation;
    private NodeInformation[][] routingTable;

    public RoutingTable(NodeInformation nodeInformation) {
        this.nodeInformation = nodeInformation;
        this.routingTable = new NodeInformation[KEY_SIZE_IN_HEX_DIGITS][POSSIBLE_DIGITS_IN_HEXADECIMAL];

        for (NodeInformation[] row : this.routingTable)
            Arrays.fill(row, null);

        String selfKey = this.nodeInformation.getKey().toString();
        int index;
        for (index = 0; index < KEY_SIZE_IN_HEX_DIGITS; index++) {
            int digitAtIndexInSelf = Character.digit(selfKey.charAt(index), 16);
            setRoutingTableEntry(index, digitAtIndexInSelf, nodeInformation);
        }
    }

    public NodeInformation[][] getRoutingTable() {
        return routingTable;
    }

    public NodeInformation[] getRow(int row) {
        return routingTable[row];
    }

    public NodeInformation get(int row, int column) {
        return routingTable[row][column];
    }

    public NodeInformation getNodeInformation() {
        return nodeInformation;
    }

    public void setRoutingTableRow(int index, NodeInformation[] row) {
        this.routingTable[index] = row;

        String selfKey = this.nodeInformation.getKey().toString();
        int digitAtIndexInSelf = Character.digit(selfKey.charAt(index), 16);
        setRoutingTableEntry(index, digitAtIndexInSelf, nodeInformation);
    }

    public void setRoutingTableEntry(int row, int column, NodeInformation nodeInformation) {
        routingTable[row][column] = nodeInformation;
    }

    public void updateRoutingTableEntry(NodeInformation node) {
        if (!node.equals(nodeInformation)) {
            int numMatchedPrefixes = prefixMatch(node.getKey());
            int firstUnmatchedDigit = Character.digit(node.getKey().toString().charAt(numMatchedPrefixes), 16);

            if (routingTable[numMatchedPrefixes][firstUnmatchedDigit] == null ||
                    Key.distanceBetween(nodeInformation.getKey(), routingTable[numMatchedPrefixes][firstUnmatchedDigit].getKey()) >
                            Key.distanceBetween(nodeInformation.getKey(), node.getKey())) {
                routingTable[numMatchedPrefixes][firstUnmatchedDigit] = node;
            }
        }
    }

    public void deleteRoutingTableEntry(NodeInformation node) {
        if (!node.equals(nodeInformation)) {
            int numMatchedPrefixes = prefixMatch(node.getKey());
            int firstUnmatchedDigit = Character.digit(node.getKey().toString().charAt(numMatchedPrefixes), 16);

            if (routingTable[numMatchedPrefixes][firstUnmatchedDigit] != null &&
                    node.equals(routingTable[numMatchedPrefixes][firstUnmatchedDigit])) {
                routingTable[numMatchedPrefixes][firstUnmatchedDigit] = null;
            }
        }
    }

    public NodeInformation lookup(Key key) {
        int numMatchedPrefixes = prefixMatch(key);
        int firstUnmatchedDigit = Character.digit(key.toString().charAt(numMatchedPrefixes), 16);

        /*if (routingTable[numMatchedPrefixes][firstUnmatchedDigit] == null) {
            int rowIndex = numMatchedPrefixes;
            NodeInformation lower = null;
            for ( ; rowIndex >= 0; rowIndex--) {
                int columnIndex = POSSIBLE_DIGITS_IN_HEXADECIMAL-1;
                if (rowIndex == numMatchedPrefixes) {
                    columnIndex = firstUnmatchedDigit;
                }

                for ( ; columnIndex>=0; columnIndex--) {
                    if (routingTable[rowIndex][columnIndex] != null && !nodeInformation.equals(routingTable[rowIndex][columnIndex])) {
                        lower = routingTable[rowIndex][columnIndex];
                        break;
                    }
                }
                if (lower != null) {
                    break;
                }
            }

            rowIndex = numMatchedPrefixes;
            NodeInformation upper = null;
            for ( ; rowIndex<KEY_SIZE_IN_HEX_DIGITS; rowIndex++) {
                int columnIndex = 0;
                if (rowIndex == numMatchedPrefixes) {
                    columnIndex = firstUnmatchedDigit;
                }

                for ( ; columnIndex<POSSIBLE_DIGITS_IN_HEXADECIMAL; columnIndex++) {
                    if (routingTable[rowIndex][columnIndex] != null && !nodeInformation.equals(routingTable[rowIndex][columnIndex])) {
                        upper = routingTable[rowIndex][columnIndex];
                        break;
                    }
                }
                if (upper != null) {
                    break;
                }
            }

            if (lower == null && upper == null) {
                return null;
            } else if (lower==null) {
                return upper;
            } else if (upper == null) {
                return lower;
            } else {
                if (Key.distanceBetween(key, lower.getKey()) > Key.distanceBetween(key, upper.getKey())) {
                    return upper;
                } else {
                    return lower;
                }
            }

        } else {*/
        return routingTable[numMatchedPrefixes][firstUnmatchedDigit];
        //}
    }

    public int prefixMatch(Key key) {
        String selfKey = nodeInformation.getKey().toString();
        String otherKey = key.toString();

        int index;
        for (index = 0; index < KEY_SIZE_IN_HEX_DIGITS; index++) {

            int digitAtIndexInSelf = Character.digit(selfKey.charAt(index), 16);
            int digitAtIndexInOther = Character.digit(otherKey.charAt(index), 16);

            if (digitAtIndexInOther != digitAtIndexInSelf) {
                break;
            }
        }
        return index;
    }

    public String toString() {
        String NEW_LINE = System.getProperty("line.separator");
        StringBuilder result = new StringBuilder("Routing Table @ " + nodeInformation.getKey().toString());
        result.append(NEW_LINE);
        int formatIndex;

        result.append("  ");
        for (formatIndex = 0; formatIndex < POSSIBLE_DIGITS_IN_HEXADECIMAL; formatIndex++) {
            result.append("  " + Integer.toHexString(formatIndex) + "  ");
        }

        result.append(NEW_LINE);
        result.append("  ");
        for (formatIndex = 0; formatIndex < POSSIBLE_DIGITS_IN_HEXADECIMAL; formatIndex++) {
            result.append("-----");
        }
        result.append("-");
        int rowIndex;
        for (rowIndex = 0; rowIndex < KEY_SIZE_IN_HEX_DIGITS; rowIndex++) {
            result.append(NEW_LINE);
            int columnIndex;
            result.append(rowIndex + " |");
            for (columnIndex = 0; columnIndex < POSSIBLE_DIGITS_IN_HEXADECIMAL; columnIndex++) {
                if (get(rowIndex, columnIndex) != null) {
                    result.append(get(rowIndex, columnIndex).getKey().toString()).append("|");
                } else {
                    result.append(" -- |");
                }
            }
            result.append(NEW_LINE);
            result.append("  ");
            for (columnIndex = 0; columnIndex < POSSIBLE_DIGITS_IN_HEXADECIMAL; columnIndex++) {
                result.append("-----");
            }
            result.append("-");
        }
        return result.toString();
    }

    public void updateRoutingTable(JoinResponse joinResponse) {
        updateRoutingTableEntry(joinResponse.getNodeInformation());
        int numMatchingPrefixes = prefixMatch(joinResponse.getKey());

        int rowIndex;
        for (rowIndex = 0; rowIndex <= numMatchingPrefixes; rowIndex++) {
            int columnIndex;
            for (columnIndex = 0; columnIndex < POSSIBLE_DIGITS_IN_HEXADECIMAL; columnIndex++) {
                if (joinResponse.getRoutingTableRows().get(rowIndex).get(columnIndex) != null) {
                    updateRoutingTableEntry(joinResponse.getRoutingTableRows().get(rowIndex).get(columnIndex));
                }
            }
        }

        if (joinResponse.getLeafSet() != null) {
            updateRoutingTableEntry(joinResponse.getLeftNeighbor());
            updateRoutingTableEntry(joinResponse.getRightNeighbor());
        }
    }

    public void updateRoutingTable(RoutingUpdate routingUpdate) {
        updateRoutingTableEntry(routingUpdate.getNodeInformation());
        int numMatchedPrefixes = prefixMatch(routingUpdate.getKey());
        int rowIndex;
        for (rowIndex = 0; rowIndex < numMatchedPrefixes; rowIndex++) {
            int columnIndex;
            for (columnIndex = 0; columnIndex < POSSIBLE_DIGITS_IN_HEXADECIMAL; columnIndex++) {
                if (routingUpdate.getRoutingTableRows().get(rowIndex).get(columnIndex) != null) {
                    updateRoutingTableEntry(routingUpdate.getRoutingTableRows().get(rowIndex).get(columnIndex));
                }
            }
        }

        if (routingUpdate.getLeafSet() != null) {
            updateRoutingTableEntry(routingUpdate.getLeftNeighbor());
            updateRoutingTableEntry(routingUpdate.getRightNeighbor());
        }
    }
}
