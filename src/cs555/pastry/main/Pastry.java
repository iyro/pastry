package cs555.pastry.main;

import cs555.pastry.exceptions.IncorrectKeySizeException;
import cs555.pastry.hashing.Key;
import cs555.pastry.nodes.DiscoveryNode;
import cs555.pastry.nodes.PeerNode;
import cs555.pastry.nodes.StoreDataNode;
import cs555.pastry.utilities.LoggingUtility;

import java.io.IOException;
import java.util.logging.Logger;

/**
 * Created by iyro on 10/3/15.
 */
public class Pastry {
    private static final Logger LOGGER = LoggingUtility.getLogger(Pastry.class.getName());
    private static type nodeType;

    public static void setNodeType(String nodeType) {
        if (nodeType.equalsIgnoreCase("D")) {
            Pastry.nodeType = type.DISCOVERY_NODE;
        } else if (nodeType.equalsIgnoreCase("P")) {
            Pastry.nodeType = type.PEER_NODE;
        } else if (nodeType.equalsIgnoreCase("S")) {
            Pastry.nodeType = type.STORE_DATA_NODE;
        } else {
            Pastry.nodeType = type.INVALID;
        }
    }

    public static String usageString() {
        String NEW_LINE = System.getProperty("line.separator");
        StringBuilder result = new StringBuilder(NEW_LINE + "Usage : ");
        result.append(NEW_LINE);
        result.append("Pastry.jar D <Port>");
        result.append(NEW_LINE);
        result.append("Pastry.jar P [<Node Identifier>] <Port> <Discovery Node IP> <Discovery Node Port> [<Node Nick>]");
        result.append(NEW_LINE);
        result.append("Pastry.jar S [<File Identifier>] <w|r> <Port> <Discovery Node IP> <Discovery Node Port> <File Path>");
        return result.toString();
    }

    public static void main(String[] args) {
        if (args.length < 1) {
            LOGGER.severe("Too few arguments");
            System.exit(1);
        }
        Pastry.setNodeType(args[0]);

        switch (Pastry.nodeType) {

            case DISCOVERY_NODE: {
                int port = 43210;

                if (args.length < 1) {
                    LOGGER.severe("Too few arguments");
                    LOGGER.warning(usageString());
                    System.exit(1);
                }

                if (args.length == 2) {
                    port = Integer.parseInt(args[1]);
                }

                DiscoveryNode discoveryNode = new DiscoveryNode(port);
                try {
                    discoveryNode.start();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            break;

            case PEER_NODE: {
                if (args.length > 6 || args.length < 4) {
                    LOGGER.severe("Too few or too many arguments");
                    LOGGER.warning(usageString());
                    System.exit(1);
                }

                PeerNode peerNode = null;
                try {
                    if (args.length == 6) { //all
                        peerNode = new PeerNode(args[1], Integer.parseInt(args[2]), args[3], Integer.parseInt(args[4]), args[5]);
                    } else if (args.length == 5) {  //No Nick
                        peerNode = new PeerNode(args[1], Integer.parseInt(args[2]), args[3], Integer.parseInt(args[4]));
                    } else {
                        peerNode = new PeerNode(Integer.parseInt(args[1]), args[2], Integer.parseInt(args[3]));
                    }
                    peerNode.start();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (ClassNotFoundException e) {
                    e.printStackTrace();
                } catch (IncorrectKeySizeException e) {
                    e.printStackTrace();
                }
            }
            break;

            case STORE_DATA_NODE: {
                if (args.length > 7 || args.length < 6) {
                    LOGGER.severe("Too few or too many arguments");
                    LOGGER.warning(usageString());
                    System.exit(1);
                }

                StoreDataNode storeDataNode = null;

                try {
                    if (args.length == 7) {
                        storeDataNode = new StoreDataNode(args[1], args[2], Integer.parseInt(args[3]), args[4], Integer.parseInt(args[5]), args[6]);
                    } else if (args.length == 6) {
                        storeDataNode = new StoreDataNode(new Key(Key.Type.FILE_NAME, args[5]).toString(), args[1], Integer.parseInt(args[2]), args[3], Integer.parseInt(args[4]), args[5]);
                    }
                    storeDataNode.start();
                } catch (IncorrectKeySizeException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (ClassNotFoundException e) {
                    e.printStackTrace();
                }
            }
            break;

            default: {
                //INVALID Node
            }
            break;
        }
    }

    private enum type {
        DISCOVERY_NODE, PEER_NODE, STORE_DATA_NODE, INVALID
    }
}
