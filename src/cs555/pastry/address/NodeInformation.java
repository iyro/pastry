package cs555.pastry.address;

import cs555.pastry.exceptions.IncorrectKeySizeException;
import cs555.pastry.hashing.Key;
import cs555.pastry.utilities.LoggingUtility;

import java.io.Serializable;
import java.util.logging.Logger;

/**
 * Created by iyro on 10/1/15.
 */
public class NodeInformation implements Serializable, Comparable<NodeInformation> {
    private static final Logger LOGGER = LoggingUtility.getLogger(NodeInformation.class.getName());
    private static final long serialVersionUID = 3880127872101812423L;
    private String nodeNickName;
    private String nodeIP;
    private int nodePort;
    private Key key;

    public NodeInformation(String nodeIP, int nodePort) {
        this.nodeIP = nodeIP;
        this.nodePort = nodePort;
        this.key = new Key();
        this.nodeNickName = "Peer_" + key.toString();
    }

    public NodeInformation(String nodeIP, int nodePort, String nodeIdentifier) throws IncorrectKeySizeException {
        this.nodeIP = nodeIP;
        this.nodePort = nodePort;
        this.key = new Key(Key.Type.KEY, nodeIdentifier);
        this.nodeNickName = "Peer_" + key.toString();
    }

    public NodeInformation(String nodeIP, int nodePort, String nodeIdentifier, String nodeNickName) throws IncorrectKeySizeException {
        this.nodeIP = nodeIP;
        this.nodePort = nodePort;
        this.key = new Key(Key.Type.KEY, nodeIdentifier);
        this.nodeNickName = nodeNickName;
    }

    public void generateNewKey() {
        this.key = new Key();
    }

    public String getNodeIP() {
        return nodeIP;
    }

    public int getNodePort() {
        return nodePort;
    }

    public Key getKey() {
        return key;
    }

    @Override
    public String toString() {
        String NEW_LINE = System.getProperty("line.separator");
        StringBuilder result = new StringBuilder("ID : " + key.toString() + ", ");
        result.append("Address : " + nodeIP + ":" + nodePort + ", ");
        result.append("Nick : " + nodeNickName);
        return result.toString();
    }

    @Override
    public int compareTo(NodeInformation nodeInformation) {
        return key.compareTo(nodeInformation.getKey());
    }

    @Override
    public int hashCode() {
        return key.hashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other instanceof NodeInformation) {
            return key.equals(((NodeInformation) other).getKey());
        } else {
            return false;
        }
    }
}
