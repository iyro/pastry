package cs555.pastry.nodes;

import cs555.pastry.address.NodeInformation;
import cs555.pastry.hashing.Key;
import cs555.pastry.packets.Packet;
import cs555.pastry.utilities.LoggingUtility;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.*;
import java.util.*;
import java.util.concurrent.ConcurrentSkipListMap;
import java.util.logging.Logger;

import static cs555.pastry.packets.Packet.packetType.*;

/**
 * Created by iyro on 9/25/15.
 */
public class DiscoveryNode {
    private static final Logger LOGGER = LoggingUtility.getLogger(DiscoveryNode.class.getName());
    private static ConcurrentSkipListMap<Key, NodeInformation> nodesInTheSystem;
    private ServerSocket serverSocket;

    public DiscoveryNode(int port) {
        try {
            this.serverSocket = new ServerSocket(port);
        } catch (IOException e) {
            throw new RuntimeException("Cannot open port " + port, e);
        }
        nodesInTheSystem = new ConcurrentSkipListMap<Key, NodeInformation>();
    }

    public void start() throws IOException {
        //LOGGER.info("Starting Discovery Node.");
        Socket clientSocket = null;

        System.setProperty("java.net.preferIPv4Stack", "true");
        Enumeration e = null;
        try {
            e = NetworkInterface.getNetworkInterfaces();
        } catch (SocketException e1) {
            e1.printStackTrace();
        }

        if (e != null) {
            while (e.hasMoreElements()) {
                NetworkInterface n = (NetworkInterface) e.nextElement();
                Enumeration ee = n.getInetAddresses();
                while (ee.hasMoreElements()) {
                    InetAddress i = (InetAddress) ee.nextElement();
                    if (i instanceof Inet6Address || i.isLoopbackAddress() || i.isSiteLocalAddress())
                        continue;
                    LOGGER.info("Discovery Node listening on " + i.getHostAddress() + ":" + serverSocket.getLocalPort());
                }
            }
        }

        new UserInputThread().start();

        while (true) {
            clientSocket = serverSocket.accept();
            new DiscoveryThread(clientSocket).start();
        }
    }

    public String listNodes() {
        String NEW_LINE = System.getProperty("line.separator");
        StringBuilder result = new StringBuilder();
        if (nodesInTheSystem.size() != 0) {
            result.append(NEW_LINE + "Active Nodes : ");
            for (Key node : nodesInTheSystem.keySet()) {
                result.append(NEW_LINE);
                result.append(nodesInTheSystem.get(node).toString());
            }
        } else {
            result.append(NEW_LINE + "No Active nodes.");
        }
        return result.toString();
    }

    private String getSortedList() {
        List<Key> sortedKeys = new ArrayList<Key>(nodesInTheSystem.size());
        sortedKeys.addAll(nodesInTheSystem.keySet());
        Collections.sort(sortedKeys);
        String NEW_LINE = System.getProperty("line.separator");
        StringBuilder result = new StringBuilder(NEW_LINE + "Sorted list of Active Nodes : ");
        for (Key key : sortedKeys) {
            result.append(NEW_LINE).append(key.toString());
        }
        return result.toString();
    }

    private static class DiscoveryThread extends Thread {
        Socket clientSocket;
        ObjectInputStream clientSocketOIS;

        public DiscoveryThread(Socket clientSocket) throws IOException {
            this.clientSocket = clientSocket;
            this.clientSocketOIS = new ObjectInputStream(clientSocket.getInputStream());
        }

        @Override
        public void run() {
            try {
                while (!clientSocket.isClosed()) {
                    Packet packet;
                    try {
                        packet = (Packet) clientSocketOIS.readObject();
                    } catch (IOException e) {
                        break;
                    }
                    handlePacket(packet);
                }
            } catch (IOException e) {
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
        }

        private synchronized void handlePacket(Packet packet) throws IOException, ClassNotFoundException {
            ObjectOutputStream clientSocketOOS = new ObjectOutputStream(clientSocket.getOutputStream());

            switch (packet.getType()) {
                case DISCOVER: {
                    NodeInformation newNode = (NodeInformation) packet.getPayload();
                    //LOGGER.info("Discovery Request from - " + newNode.getKey().toString());
                    if (nodesInTheSystem.containsKey(newNode.getKey())) {
                        //Key collision
                        Packet failurePacket = new Packet(DISCOVER_FAIL, null);
                        clientSocketOOS.writeObject(failurePacket);
                    } else {
                        Random generator = new Random();
                        Object[] values = nodesInTheSystem.values().toArray();
                        NodeInformation randomNode;
                        if (values.length != 0) {
                            randomNode = (NodeInformation) values[generator.nextInt(values.length)];
                            LOGGER.info("Random Peer for " + newNode.getKey().toString() + " - " + randomNode.toString());
                        } else {
                            randomNode = null;
                        }

                        Packet successPacket = new Packet(DISCOVER_SUCCESS, randomNode);
                        clientSocketOOS.writeObject(successPacket);
                        if (randomNode == null) {
                            LOGGER.info("First Node - " + newNode.toString());
                            nodesInTheSystem.put(newNode.getKey(), newNode);
                        } else {
                            LOGGER.info("New Node - " + newNode.toString());
                            Packet ackPacket = (Packet) clientSocketOIS.readObject();

                            if (ackPacket.getType() == ADD_TO_DHT) {
                                nodesInTheSystem.put(newNode.getKey(), newNode);
                            }
                        }
                    }
                }
                break;
                case DISCOVER_FILE: {
                    NodeInformation fileInformation = (NodeInformation) packet.getPayload();
                    LOGGER.info("Discover File Request for - " + fileInformation.getKey().toString());
                    if (nodesInTheSystem.containsKey(fileInformation.getKey())) {
                        //Key collision
                        Packet failurePacket = new Packet(DISCOVER_FAIL, null);
                        clientSocketOOS.writeObject(failurePacket);
                    } else {
                        Random generator = new Random();
                        Object[] values = nodesInTheSystem.values().toArray();
                        NodeInformation randomNode;
                        if (values.length != 0) {
                            randomNode = (NodeInformation) values[generator.nextInt(values.length)];
                            LOGGER.info("Random Peer - " + randomNode.getKey().toString());
                        } else {
                            randomNode = null;
                        }
                        Packet successPacket = new Packet(DISCOVER_SUCCESS, randomNode);
                        clientSocketOOS.writeObject(successPacket);
                    }
                }
                break;
                case KILL_NODE: {
                    NodeInformation node = (NodeInformation) packet.getPayload();
                    LOGGER.info("Remove Request for - " + node.getKey().toString());

                    if (nodesInTheSystem.remove(node.getKey()) != null) {
                        Packet killPacket = new Packet(KILLED_NODE, null);
                        clientSocketOOS.writeObject(killPacket);
                    } else {
                        Packet killPacket = new Packet(KILL_FAILED, null);
                        clientSocketOOS.writeObject(killPacket);
                    }
                }
                default: {

                }
                break;
            }
        }
    }

    private class UserInputThread extends Thread {
        @Override
        public void run() {
            Scanner inputScanner = new Scanner(System.in);
            while (true) {
                for (char c : inputScanner.next().toCharArray()) {
                    switch (c) {
                        case 'l':
                        case 'L': {
                            LOGGER.info(listNodes());
                        }
                        break;
                        case 'Q':
                        case 'q': {
                            System.exit(0);
                        }
                        break;
                        default: {
                            LOGGER.warning("Invalid Input : " + c);
                        }
                    }
                }
            }
        }
    }
}
