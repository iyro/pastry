package cs555.pastry.nodes;

import cs555.pastry.address.NodeInformation;
import cs555.pastry.exceptions.IncorrectKeySizeException;
import cs555.pastry.hashing.Key;
import cs555.pastry.items.DataChunk;
import cs555.pastry.items.DataItem;
import cs555.pastry.packets.*;
import cs555.pastry.utilities.LoggingUtility;

import java.io.*;
import java.net.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.logging.Logger;

import static cs555.pastry.packets.Packet.packetType.*;
import static cs555.pastry.utilities.Constants.CHUNK_SIZE;

/**
 * Created by iyro on 9/25/15.
 */
public class StoreDataNode {
    private static final Logger LOGGER = LoggingUtility.getLogger(StoreDataNode.class.getName());
    private static operationType operation;
    private static String discoveryNodeIP;
    private static int discoveryNodePort;
    private static ServerSocket serverSocket;
    private static NodeInformation nodeInformation;
    private static String fileName;

    public StoreDataNode(String key, String operation, int port, String discoveryNodeIP, int discoveryNodePort, String fileName) throws IOException, IncorrectKeySizeException {
        StoreDataNode.discoveryNodeIP = discoveryNodeIP;
        StoreDataNode.discoveryNodePort = discoveryNodePort;
        StoreDataNode.serverSocket = new ServerSocket(port);

        StoreDataNode.nodeInformation = new NodeInformation(getIPAddress(), serverSocket.getLocalPort(), key);

        StoreDataNode.fileName = fileName;

        if (operation.charAt(0) == 'w') {
            StoreDataNode.operation = operationType.WRITE;
        } else if (operation.charAt(0) == 'r') {
            StoreDataNode.operation = operationType.READ;
        } else {
            StoreDataNode.operation = operationType.INVALID;
        }
    }

    private String getIPAddress() {
        System.setProperty("java.net.preferIPv4Stack", "true");
        Enumeration e = null;
        try {
            e = NetworkInterface.getNetworkInterfaces();
        } catch (SocketException e1) {
            e1.printStackTrace();
        }

        while (e != null && e.hasMoreElements()) {
            NetworkInterface n = (NetworkInterface) e.nextElement();
            Enumeration ee = n.getInetAddresses();
            while (ee.hasMoreElements()) {
                InetAddress i = (InetAddress) ee.nextElement();
                if (i instanceof Inet6Address || i.isLoopbackAddress() || i.isSiteLocalAddress())
                    continue;
                return i.getHostAddress();
            }
        }

        return "localhost";
    }

    private String printPath(ArrayList<Key> path) {
        String NEW_LINE = System.getProperty("line.separator");
        StringBuilder result = new StringBuilder("File Request Path : ");
        boolean first = true;
        for (Key key : path) {
            if (!first) {
                result.append("->");
            }
            first = false;
            result.append(key.toString());
        }
        return result.toString();
    }

    public void start() throws IOException, ClassNotFoundException {
        Socket discoverySocket = new Socket(discoveryNodeIP, discoveryNodePort);
        ObjectOutputStream discoverySocketOOS = new ObjectOutputStream(discoverySocket.getOutputStream());

        boolean collision = true;
        int retries = 0;

        while (collision && retries < 5) {
            LOGGER.info("Key : " + nodeInformation.getKey().toString());
            Packet discoverRequest = new Packet(DISCOVER_FILE, nodeInformation);
            discoverySocketOOS.writeObject(discoverRequest);
            ObjectInputStream discoverySocketOIS = new ObjectInputStream(discoverySocket.getInputStream());
            Packet discoverResponse = (Packet) discoverySocketOIS.readObject();

            switch (discoverResponse.getType()) {

                case DISCOVER_SUCCESS: {
                    collision = false;
                    NodeInformation entryNode = (NodeInformation) discoverResponse.getPayload();

                    new clientWorkerThread().start();

                    if (entryNode != null) {
                        LOGGER.info("Random Node : " + entryNode.getKey().toString());
                        Socket entryNodeSocket = new Socket(entryNode.getNodeIP(), entryNode.getNodePort());
                        ObjectOutputStream entryNodeSocketOOS = new ObjectOutputStream(entryNodeSocket.getOutputStream());
                        Packet fileRequest = null;

                        if (operation == operationType.READ) {
                            fileRequest = new Packet(FILE_READ_REQUEST, new FileRequest(nodeInformation));
                        } else if (operation == operationType.WRITE) {
                            fileRequest = new Packet(FILE_WRITE_REQUEST, new FileRequest(nodeInformation));
                        }

                        entryNodeSocketOOS.writeObject(fileRequest);
                        entryNodeSocket.close();
                    } else {
                        LOGGER.warning("No nodes in the system. Try again later.");
                        System.exit(0);
                    }

                    discoverySocket.close();
                }
                break;

                case DISCOVER_FAIL: {
                    collision = true;
                    discoverySocketOOS.reset();
                    nodeInformation.generateNewKey();
                    retries++;
                    LOGGER.info("Key Collision. Retrying with key : " + nodeInformation.getKey().toString() + " (Try " + (retries + 1) + " of 5)");
                }
                break;
            }
        }
    }

    private enum operationType {
        WRITE, READ, INVALID
    }

    private class clientWorkerThread extends Thread {
        @Override
        public void run() {
            ObjectInputStream clientSocketOIS;
            Packet request;

            try {
                Socket clientSocket = serverSocket.accept();
                clientSocketOIS = new ObjectInputStream(clientSocket.getInputStream());

                request = (Packet) clientSocketOIS.readObject();

                switch (request.getType()) {
                    case FILE_WRITE_RESPONSE: {
                        FileWriteResponse fileResponse = (FileWriteResponse) request.getPayload();
                        LOGGER.info(printPath(fileResponse.getPath()));
                        File file = new File(fileName);
                        FileInputStream fis = new FileInputStream(file);
                        long size = fis.getChannel().size();
                        long totalChunks = size / CHUNK_SIZE;

                        if (size % CHUNK_SIZE != 0) {
                            totalChunks++;
                        }

                        DataItem dataItem = new DataItem(nodeInformation.getKey(), null, fileName, size);

                        Socket fileWriteSocket = new Socket(fileResponse.getNodeInformation().getNodeIP(), fileResponse.getNodeInformation().getNodePort());
                        ObjectOutputStream fileWriteSocketOOS = new ObjectOutputStream(fileWriteSocket.getOutputStream());

                        fileWriteSocketOOS.writeObject(new Packet(FILE_INFO, dataItem));

                        long chunkID;
                        boolean done = true;
                        for (chunkID = 0; chunkID < totalChunks; chunkID++) {
                            DataChunk dataChunk = new DataChunk(fileName, size, chunkID);
                            if (dataChunk.read()) {
                                fileWriteSocketOOS.writeObject(new Packet(FILE_CHUNK, dataChunk));
                                //LOGGER.info("File chunk " + (chunkID + 1) + " written to " + fileResponse.getNodeInformation().getKey().toString());
                            } else {
                                fileWriteSocketOOS.writeObject(new Packet(FILE_CHUNK_WRITE_FAILED, null));
                                done = false;
                                LOGGER.severe("File read failed.");
                                break;
                            }
                            fileWriteSocketOOS.flush();
                            fileWriteSocketOOS.reset();
                        }

                        if (done) {
                            ObjectInputStream fileWriteSocketOIS = new ObjectInputStream(fileWriteSocket.getInputStream());
                            Packet ackPacket = (Packet) fileWriteSocketOIS.readObject();

                            if (ackPacket.getType() == FILE_WRITE_SUCCESSFUL) {
                                LOGGER.info("File " + fileName + " successfuly written at " + fileResponse.getKey().toString());
                            } else {
                                LOGGER.severe("File " + fileName + " could not be written at " + fileResponse.getKey().toString());
                            }
                        } else {
                            LOGGER.severe("File " + fileName + " could not be written at " + fileResponse.getKey().toString());
                        }
                        fileWriteSocket.close();
                    }
                    break;
                    case FILE_INFO: {
                        long chunkID;
                        FileAndPathInfo dataItem = (FileAndPathInfo) request.getPayload();
                        LOGGER.info(printPath(dataItem.getPath()));
                        long size = dataItem.getFileSize();
                        boolean done = true;
                        long totalChunks = size / CHUNK_SIZE;

                        if (size % CHUNK_SIZE != 0) {
                            totalChunks++;
                        }

                        Path p = Paths.get(dataItem.getName());
                        Files.deleteIfExists(p);

                        for (chunkID = 0; chunkID < totalChunks; chunkID++) {
                            request = (Packet) clientSocketOIS.readObject();
                            if (request.getType() == FILE_CHUNK) {
                                DataChunk dataChunk = (DataChunk) request.getPayload();
                                if (dataChunk.write()) {

                                } else {
                                    done = false;
                                    LOGGER.severe("File chunk " + (chunkID + 1) + " could not be written to disk.");
                                    break;
                                }
                            } else {
                                done = false;
                                LOGGER.severe("File read failed.");
                                break;
                            }
                        }

                        if (done) {
                            ObjectOutputStream clientSocketOOS = new ObjectOutputStream(clientSocket.getOutputStream());
                            clientSocketOOS.writeObject(new Packet(FILE_WRITE_SUCCESSFUL, null));

                            LOGGER.info("File " + dataItem.getName() + " successfuly written to disk from " + dataItem.getNodeInformation().getKey().toString());
                        }
                    }
                    break;
                    case FILE_NOT_FOUND: {
                        NodeInfoAndPath node = (NodeInfoAndPath) request.getPayload();
                        LOGGER.info(printPath(node.getPath()));
                        LOGGER.severe("File with identifier " + nodeInformation.getKey().toString() + " not found @ " + node.getNodeInformation().getKey().toString());
                    }
                    break;
                    default: {
                        LOGGER.severe("Invalid Packet.");
                    }
                }
            } catch (UnknownHostException e) {
                e.printStackTrace();
            } catch (FileNotFoundException e) {
                LOGGER.severe("No such file.");
            } catch (IOException e) {
                LOGGER.severe("Error");
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
        }
    }
}