package cs555.pastry.nodes;

import cs555.pastry.address.NodeInformation;
import cs555.pastry.exceptions.IncorrectKeySizeException;
import cs555.pastry.exceptions.ReceivedMyOwnKeyException;
import cs555.pastry.hashing.Key;
import cs555.pastry.items.DataChunk;
import cs555.pastry.items.DataItem;
import cs555.pastry.packets.*;
import cs555.pastry.routing.LeafSet;
import cs555.pastry.routing.RoutingTable;
import cs555.pastry.utilities.LoggingUtility;

import java.io.*;
import java.net.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Logger;

import static cs555.pastry.packets.Packet.packetType.*;
import static cs555.pastry.utilities.Constants.*;

/**
 * Created by iyro on 9/25/15.
 */
public class PeerNode {
    private static final Object sharedLock = new Object();
    private static final Logger LOGGER = LoggingUtility.getLogger(PeerNode.class.getName());
    private static ServerSocket serverSocket;
    private static NodeInformation nodeInformation;
    private static String discoveryNodeIP;
    private static int discoveryNodePort;
    private static RoutingTable routingTable;
    private static LeafSet leafSet;
    private static ConcurrentHashMap<Key, DataItem> filesOnThisNode;

    public PeerNode(String key, int port, String discoveryNodeIP, int discoveryNodePort) throws IOException, IncorrectKeySizeException {
        PeerNode.discoveryNodeIP = discoveryNodeIP;
        PeerNode.discoveryNodePort = discoveryNodePort;
        PeerNode.serverSocket = new ServerSocket(port);

        PeerNode.nodeInformation = new NodeInformation(getIPAddress(), serverSocket.getLocalPort(), key);

        PeerNode.routingTable = new RoutingTable(PeerNode.nodeInformation);
        PeerNode.leafSet = new LeafSet(PeerNode.nodeInformation);
        PeerNode.filesOnThisNode = new ConcurrentHashMap<>();
    }

    public PeerNode(int port, String discoveryNodeIP, int discoveryNodePort) throws IOException, IncorrectKeySizeException {
        PeerNode.discoveryNodeIP = discoveryNodeIP;
        PeerNode.discoveryNodePort = discoveryNodePort;
        PeerNode.serverSocket = new ServerSocket(port);

        PeerNode.nodeInformation = new NodeInformation(getIPAddress(), serverSocket.getLocalPort());

        PeerNode.routingTable = new RoutingTable(PeerNode.nodeInformation);
        PeerNode.leafSet = new LeafSet(PeerNode.nodeInformation);
        PeerNode.filesOnThisNode = new ConcurrentHashMap<>();
    }

    public PeerNode(String key, int port, String discoveryNodeIP, int discoveryNodePort, String nickname) throws IOException, IncorrectKeySizeException {
        PeerNode.discoveryNodeIP = discoveryNodeIP;
        PeerNode.discoveryNodePort = discoveryNodePort;
        PeerNode.serverSocket = new ServerSocket(port);

        PeerNode.nodeInformation = new NodeInformation(getIPAddress(), serverSocket.getLocalPort(), key, nickname);

        PeerNode.routingTable = new RoutingTable(PeerNode.nodeInformation);
        PeerNode.leafSet = new LeafSet(PeerNode.nodeInformation);
        PeerNode.filesOnThisNode = new ConcurrentHashMap<>();
    }

    private String getIPAddress() {
        System.setProperty("java.net.preferIPv4Stack", "true");
        Enumeration e = null;
        try {
            e = NetworkInterface.getNetworkInterfaces();
        } catch (SocketException e1) {
            e1.printStackTrace();
        }

        while (e != null && e.hasMoreElements()) {
            NetworkInterface n = (NetworkInterface) e.nextElement();
            Enumeration ee = n.getInetAddresses();
            while (ee.hasMoreElements()) {
                InetAddress i = (InetAddress) ee.nextElement();
                if (i instanceof Inet6Address || i.isLoopbackAddress() || i.isSiteLocalAddress())
                    continue;
                return i.getHostAddress();
            }
        }

        return "localhost";
    }

    public void start() throws IOException, ClassNotFoundException {
        new DiscoveryThread().start();

        new UserInputThread().start();

        while (serverSocket.isBound()) {
            Socket clientSocket = serverSocket.accept();
            new PeerNodeWorkerThread(clientSocket).start();
        }
    }

    private NodeInformation lookup(Key key) throws IOException, ReceivedMyOwnKeyException {
        NodeInformation dispatchNode = leafSet.lookup(key);

        if (dispatchNode == null) {
            dispatchNode = routingTable.lookup(key);
        }

        if (dispatchNode == null) {
            ArrayList<NodeInformation> sortListNodes = new ArrayList<>();

            int rowIndex = routingTable.prefixMatch(key);
            for (; rowIndex < KEY_SIZE_IN_HEX_DIGITS; rowIndex++) {
                int columnIndex = 0;
                for (; columnIndex < POSSIBLE_DIGITS_IN_HEXADECIMAL; columnIndex++) {
                    if (routingTable.get(rowIndex, columnIndex) != null &&
                            !routingTable.get(rowIndex, columnIndex).equals(nodeInformation) &&
                            !sortListNodes.contains(routingTable.get(rowIndex, columnIndex))) {
                        sortListNodes.add(routingTable.get(rowIndex, columnIndex));
                    }
                }
            }

            if (leafSet.getLeftNeighbor() != null &&
                    !leafSet.getLeftNeighbor().equals(nodeInformation) &&
                    !sortListNodes.contains(leafSet.getLeftNeighbor())) {
                sortListNodes.add(leafSet.getLeftNeighbor());
            }

            if (leafSet.getRightNeighbor() != null &&
                    !leafSet.getRightNeighbor().equals(nodeInformation) &&
                    !sortListNodes.contains(leafSet.getRightNeighbor())) {
                sortListNodes.add(leafSet.getRightNeighbor());
            }

            for (NodeInformation node : sortListNodes) {
                if (dispatchNode == null || Key.distanceBetween(node.getKey(), key) < Key.distanceBetween(dispatchNode.getKey(), key)) {
                    dispatchNode = node;
                }
            }
        }

        return dispatchNode;
    }

    private String displayFileList() {
        String NEW_LINE = System.getProperty("line.separator");
        StringBuilder result = new StringBuilder("List of Files Stored @ " + nodeInformation.getKey().toString() + " : ");
        if (filesOnThisNode.size() != 0) {
            for (Key file : filesOnThisNode.keySet()) {
                result.append(NEW_LINE);
                result.append(filesOnThisNode.get(file).toString());
            }
        } else {
            result.append(NEW_LINE).append("No files here.");
        }
        return result.toString();
    }

    private synchronized void quitGracefully() {
        try {
            Socket discoverySocket = new Socket(discoveryNodeIP, discoveryNodePort);
            ObjectOutputStream discoverySocketOOS = new ObjectOutputStream(discoverySocket.getOutputStream());

            LOGGER.info("Gracefully dying!");
            Packet discoverRequest = new Packet(KILL_NODE, nodeInformation);
            discoverySocketOOS.writeObject(discoverRequest);
            ObjectInputStream discoverySocketOIS = new ObjectInputStream(discoverySocket.getInputStream());
            Packet discoverResponse = (Packet) discoverySocketOIS.readObject();

            if (discoverResponse.getType() == KILLED_NODE) {
                discoverySocket.close();
                ArrayList<NodeInformation> sendUpdateTo = new ArrayList<>();
                for (NodeInformation otherNode : leafSet.getLeafSet()) {
                    if (otherNode != null && !otherNode.getKey().equals(nodeInformation.getKey()) &&
                            !sendUpdateTo.contains(otherNode)) {
                        sendUpdateTo.add(otherNode);
                    }
                }

                for (NodeInformation[] row : routingTable.getRoutingTable()) {
                    for (NodeInformation otherNode : row) {
                        if (otherNode != null && !otherNode.getKey().equals(nodeInformation.getKey()) &&
                                !sendUpdateTo.contains(otherNode)) {
                            sendUpdateTo.add(otherNode);
                        }
                    }
                }

                for (NodeInformation otherNode : sendUpdateTo) {
                    try {

                        Socket otherNodeSocket = new Socket(otherNode.getNodeIP(), otherNode.getNodePort());
                        ObjectOutputStream otherNodeSocketOOS = new ObjectOutputStream(otherNodeSocket.getOutputStream());

                        RoutingUpdate routingUpdate = new RoutingUpdate(nodeInformation);
                        int numMatchingPrefixes = routingTable.prefixMatch(otherNode.getKey());

                        int index;
                        for (index = 0; index <= numMatchingPrefixes; index++) {
                            routingUpdate.addToRoutingTableRows(new ArrayList<>(Arrays.asList(routingTable.getRow(index))));
                        }

                        if (otherNode.equals(leafSet.getLeftNeighbor()) || otherNode.equals(leafSet.getRightNeighbor())) {
                            routingUpdate.setLeafSet(leafSet.getLeafSet());
                        }

                        Packet routingUpdatePacket = new Packet(REMOVE_NODE, routingUpdate);
                        otherNodeSocketOOS.writeObject(routingUpdatePacket);
                        otherNodeSocket.close();
                    } catch (Exception e) {
                        continue;
                    }
                }

                for (Key key : filesOnThisNode.keySet()) {
                    DataItem dataItem = filesOnThisNode.get(key);

                    long size = dataItem.getFileSize();
                    long totalChunks = size / CHUNK_SIZE;

                    if (size % CHUNK_SIZE != 0) {
                        totalChunks++;
                    }

                    NodeInformation dispatchNode = leafSet.lookupExcludingSelf(key);
                    if (!dispatchNode.equals(nodeInformation)) {
                        Socket fileResponseSocket = new Socket(dispatchNode.getNodeIP(), dispatchNode.getNodePort());
                        ObjectOutputStream fileResponseSocketOOS = new ObjectOutputStream(fileResponseSocket.getOutputStream());

                        fileResponseSocketOOS.writeObject(new Packet(FILE_INFO, dataItem));
                        long chunkID;
                        boolean done = true;
                        for (chunkID = 0; chunkID < totalChunks; chunkID++) {
                            DataChunk dataChunk = new DataChunk(dataItem.getName(), dataItem.getFileSize(), chunkID);
                            if (dataChunk.readDHT()) {
                                fileResponseSocketOOS.writeObject(new Packet(FILE_CHUNK, dataChunk));
                                //LOGGER.info("File chunk " + (chunkID+1) + " written to " + dispatchNode.getKey().toString());
                            } else {
                                fileResponseSocketOOS.writeObject(new Packet(FILE_CHUNK_WRITE_FAILED, null));
                                done = false;
                                LOGGER.severe("File read failed.");
                                break;
                            }

                            fileResponseSocketOOS.flush();
                            fileResponseSocketOOS.reset();
                        }

                        if (done) {
                            ObjectInputStream fileResponseSocketOIS = new ObjectInputStream(fileResponseSocket.getInputStream());
                            Packet ackPacket = (Packet) fileResponseSocketOIS.readObject();

                            if (ackPacket.getType() == FILE_WRITE_SUCCESSFUL) {
                                LOGGER.info("File " + dataItem.getName() + " successfuly migrated to " + dispatchNode.getKey().toString());
                            } else {
                                LOGGER.severe("File " + dataItem.getName() + " could not be migrated to " + dispatchNode.getKey().toString());
                            }
                        } else {
                            LOGGER.severe("File " + dataItem.getName() + " could not be migrated to " + dispatchNode.getKey().toString());
                        }
                        fileResponseSocket.close();
                    } else {
                        LOGGER.info("Couldn't migrate file " + filesOnThisNode.get(key).getName());
                    }
                }
            } else {
                discoverySocket.close();
                LOGGER.severe("Discovery Node couldn't remove. Dying anyway!");
            }
            System.exit(0);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (UnknownHostException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private String printJoinPath(ArrayList<Key> path) {
        String NEW_LINE = System.getProperty("line.separator");
        StringBuilder result = new StringBuilder("Join Request Path : ");
        boolean first = true;
        for (Key key : path) {
            if (!first) {
                result.append("->");
            }
            first = false;
            result.append(key.toString());
        }
        return result.toString();
    }

    private class DiscoveryThread extends Thread {
        @Override
        public void run() {
            boolean collision = true;
            int retries = 0;

            while (collision && retries < 5) {
                LOGGER.info("Key : " + nodeInformation.getKey().toString());
                Packet discoverRequest = new Packet(DISCOVER, nodeInformation);

                try {
                    Socket discoverySocket = new Socket(discoveryNodeIP, discoveryNodePort);
                    ObjectOutputStream discoverySocketOOS = new ObjectOutputStream(discoverySocket.getOutputStream());

                    discoverySocketOOS.writeObject(discoverRequest);
                    ObjectInputStream discoverySocketOIS = new ObjectInputStream(discoverySocket.getInputStream());
                    Packet discoverResponse = (Packet) discoverySocketOIS.readObject();

                    switch (discoverResponse.getType()) {

                        case DISCOVER_SUCCESS: {
                            collision = false;
                            NodeInformation entryNode = (NodeInformation) discoverResponse.getPayload();

                            if (entryNode != null) {
                                LOGGER.info("Random Node : " + entryNode.getKey().toString());
                                Socket entryNodeSocket = new Socket(entryNode.getNodeIP(), entryNode.getNodePort());
                                ObjectOutputStream entryNodeSocketOOS = new ObjectOutputStream(entryNodeSocket.getOutputStream());
                                Packet joinRequest = new Packet(JOIN_REQUEST, new JoinRequest(nodeInformation));
                                entryNodeSocketOOS.writeObject(joinRequest);
                                entryNodeSocketOOS.close();

                                synchronized (sharedLock) {
                                    sharedLock.wait();
                                    discoverySocketOOS.writeObject(new Packet(ADD_TO_DHT, null));
                                }

                            } else {
                                LOGGER.info("First Node.");
                                LOGGER.info(routingTable.toString());
                                LOGGER.info(leafSet.toString());
                            }

                            discoverySocket.close();
                        }
                        break;

                        case DISCOVER_FAIL: {
                            collision = true;
                            discoverySocketOOS.reset();
                            nodeInformation.generateNewKey();
                            retries++;
                            LOGGER.info("Key Collision. Retrying with key : " + nodeInformation.getKey().toString() + " (Try " + (retries + 1) + " of 5)");
                        }
                        break;
                    }
                } catch (ClassNotFoundException e) {
                    LOGGER.severe("Error in Discovery.");
                    System.exit(1);
                } catch (UnknownHostException e) {
                    LOGGER.severe("Error in Discovery.");
                    System.exit(1);
                } catch (IOException e) {
                    LOGGER.severe("Error in Discovery.");
                    System.exit(1);
                } catch (InterruptedException e) {
                    LOGGER.severe("Interrupted exception in Discovery.");
                }
            }
        }
    }

    private class PeerNodeWorkerThread extends Thread {
        private Socket clientSocket;

        public PeerNodeWorkerThread(Socket clientSocket) throws IOException {
            this.clientSocket = clientSocket;
        }

        @Override
        public void run() {
            ObjectInputStream clientSocketOIS;
            Packet request;

            try {
                clientSocketOIS = new ObjectInputStream(clientSocket.getInputStream());
                request = (Packet) clientSocketOIS.readObject();

                switch (request.getType()) {
                    case JOIN_REQUEST:
                        synchronized (sharedLock) {
                            JoinRequest joinRequest = (JoinRequest) request.getPayload();
                            joinRequest.incrementHopCount();
                            joinRequest.addToPath(nodeInformation.getKey());
                            LOGGER.info("Join Request for " + joinRequest.getKey().toString() + ". Hop Count : " + joinRequest.getHopCount());

                            JoinResponse joinResponse = new JoinResponse(nodeInformation);
                            int numMatchingPrefixes = routingTable.prefixMatch(joinRequest.getKey());

                            int index;
                            for (index = 0; index <= numMatchingPrefixes; index++) {
                                joinResponse.addToRoutingTableRows(new ArrayList<>(Arrays.asList(routingTable.getRow(index))));
                            }

                            //lookup
                            NodeInformation dispatchNode = lookup(joinRequest.getKey());

                            if (dispatchNode.getKey().equals(nodeInformation.getKey())) {
                                joinResponse.setLeafSet(leafSet.getLeafSet());
                                joinResponse.initializeAndSetPath(joinRequest.getPath());
                            }


                            Socket joinResponseSocket = new Socket(joinRequest.getNodeInformation().getNodeIP(), joinRequest.getNodeInformation().getNodePort());
                            ObjectOutputStream joinResponseSocketOOS = new ObjectOutputStream(joinResponseSocket.getOutputStream());
                            joinResponseSocketOOS.writeObject(new Packet(JOIN_RESPONSE, joinResponse));

                            ObjectInputStream joinResponseSocketOIS = new ObjectInputStream(joinResponseSocket.getInputStream());
                            Packet ackPacket = (Packet) joinResponseSocketOIS.readObject();

                            if (ackPacket.getType() == JOIN_RESPONSE_PROCESSED) {
                                if (!dispatchNode.getKey().equals(nodeInformation.getKey())) {
                                    while (true) {
                                        try {
                                            Socket joinRequestSocket = new Socket(dispatchNode.getNodeIP(), dispatchNode.getNodePort());
                                            ObjectOutputStream joinRequestSocketOOS = new ObjectOutputStream(joinRequestSocket.getOutputStream());
                                            joinRequestSocketOOS.writeObject(new Packet(JOIN_REQUEST, joinRequest));
                                            LOGGER.info("Join Request (" + joinRequest.getKey().toString() + ") dispatched to : " + dispatchNode.getKey().toString());
                                            joinRequestSocket.close();
                                            break;
                                        } catch (IOException e) {
                                            routingTable.deleteRoutingTableEntry(dispatchNode);
                                            dispatchNode = lookup(joinRequest.getKey());
                                        }
                                    }
                                }
                            }
                            joinResponseSocket.close();
                        }
                        break;
                    case JOIN_RESPONSE:
                        synchronized (sharedLock) {
                            JoinResponse joinResponse = (JoinResponse) request.getPayload();
                            //LOGGER.info("Join Response from " + joinResponse.getKey().toString());

                            routingTable.updateRoutingTable(joinResponse);
                            leafSet.updateLeafSet(joinResponse);

                            if (joinResponse.getLeafSet() != null) {

                                LOGGER.info(routingTable.toString());
                                LOGGER.info(leafSet.toString());

                                LOGGER.info(printJoinPath(joinResponse.getPath()));

                                ArrayList<NodeInformation> sendUpdateTo = new ArrayList<>();
                                for (NodeInformation otherNode : leafSet.getLeafSet()) {
                                    if (otherNode != null && !otherNode.getKey().equals(nodeInformation.getKey()) &&
                                            !sendUpdateTo.contains(otherNode)) {
                                        sendUpdateTo.add(otherNode);
                                    }
                                }

                                for (NodeInformation[] row : routingTable.getRoutingTable()) {
                                    for (NodeInformation otherNode : row) {
                                        if (otherNode != null && !otherNode.getKey().equals(nodeInformation.getKey()) &&
                                                !sendUpdateTo.contains(otherNode)) {
                                            sendUpdateTo.add(otherNode);
                                        }
                                    }
                                }

                                for (NodeInformation otherNode : sendUpdateTo) {
                                    Socket otherNodeSocket = new Socket(otherNode.getNodeIP(), otherNode.getNodePort());
                                    ObjectOutputStream otherNodeSocketOOS = new ObjectOutputStream(otherNodeSocket.getOutputStream());

                                    RoutingUpdate routingUpdate = new RoutingUpdate(nodeInformation);
                                    int numMatchingPrefixes = routingTable.prefixMatch(otherNode.getKey());

                                    int index;
                                    for (index = 0; index <= numMatchingPrefixes; index++) {
                                        routingUpdate.addToRoutingTableRows(new ArrayList<>(Arrays.asList(routingTable.getRow(index))));
                                    }

                                    if (otherNode.equals(leafSet.getLeftNeighbor()) || otherNode.equals(leafSet.getRightNeighbor())) {
                                        routingUpdate.setLeafSet(leafSet.getLeafSet());
                                    }

                                    Packet routingUpdatePacket = new Packet(ROUTING_UPDATE, routingUpdate);
                                    otherNodeSocketOOS.writeObject(routingUpdatePacket);
                                    otherNodeSocket.close();
                                }

                                sharedLock.notifyAll();
                            }

                            ObjectOutputStream clientSocketOOS = new ObjectOutputStream(clientSocket.getOutputStream());
                            clientSocketOOS.writeObject(new Packet(JOIN_RESPONSE_PROCESSED, null));
                        }
                        break;
                    case REMOVE_NODE:
                        synchronized (sharedLock) {
                            RoutingUpdate otherNodeRoutingInfo = (RoutingUpdate) request.getPayload();
                            LOGGER.info("Routing Update from " + otherNodeRoutingInfo.getKey().toString());

                            routingTable.deleteRoutingTableEntry(otherNodeRoutingInfo.getNodeInformation());
                            leafSet.deleteLeafSetEntry(otherNodeRoutingInfo.getNodeInformation());
                            leafSet.updateLeafSetDelete(otherNodeRoutingInfo);

                            LOGGER.info(routingTable.toString());
                            LOGGER.info(leafSet.toString());
                        }
                        break;
                    case ROUTING_UPDATE:
                        synchronized (sharedLock) {
                            RoutingUpdate otherNodeRoutingInfo = (RoutingUpdate) request.getPayload();
                            LOGGER.info("Routing Update from " + otherNodeRoutingInfo.getKey().toString());

                            routingTable.updateRoutingTable(otherNodeRoutingInfo);
                            leafSet.updateLeafSet(otherNodeRoutingInfo);

                            for (Iterator<Key> iterator = filesOnThisNode.keySet().iterator(); iterator.hasNext(); ) {
                                Key key = iterator.next();
                                DataItem dataItem = filesOnThisNode.get(key);

                                long size = dataItem.getFileSize();
                                long totalChunks = size / CHUNK_SIZE;

                                if (size % CHUNK_SIZE != 0) {
                                    totalChunks++;
                                }

                                NodeInformation dispatchNode = lookup(key);

                                if (dispatchNode != null && !dispatchNode.equals(nodeInformation)) {
                                    while (true) {
                                        try {
                                            Socket fileResponseSocket = new Socket(dispatchNode.getNodeIP(), dispatchNode.getNodePort());
                                            ObjectOutputStream fileResponseSocketOOS = new ObjectOutputStream(fileResponseSocket.getOutputStream());
                                            dataItem.setNodeInformation(null);
                                            fileResponseSocketOOS.writeObject(new Packet(FILE_INFO, dataItem));
                                            long chunkID;
                                            boolean done = true;
                                            for (chunkID = 0; chunkID < totalChunks; chunkID++) {
                                                DataChunk dataChunk = new DataChunk(dataItem.getName(), dataItem.getFileSize(), chunkID);

                                                if (dataChunk.readDHT()) {
                                                    fileResponseSocketOOS.writeObject(new Packet(FILE_CHUNK, dataChunk));
                                                    //LOGGER.info("File chunk " + (chunkID + 1) + " written to " + dispatchNode.getKey().toString());
                                                } else {
                                                    fileResponseSocketOOS.writeObject(new Packet(FILE_CHUNK_WRITE_FAILED, null));
                                                    done = false;
                                                    LOGGER.severe("File read failed.");
                                                    break;
                                                }
                                                fileResponseSocketOOS.flush();
                                                fileResponseSocketOOS.reset();
                                            }
                                            if (done) {
                                                ObjectInputStream fileResponseSocketOIS = new ObjectInputStream(fileResponseSocket.getInputStream());
                                                Packet ackPacket = (Packet) fileResponseSocketOIS.readObject();

                                                if (ackPacket.getType() == FILE_WRITE_SUCCESSFUL) {
                                                    LOGGER.info("File " + dataItem.getName() + " successfuly migrated to " + dispatchNode.getKey().toString());
                                                } else {
                                                    LOGGER.severe("File " + dataItem.getName() + " could not be migrated to " + dispatchNode.getKey().toString());
                                                }
                                                filesOnThisNode.remove(key);
                                                Path p = Paths.get(dataItem.getDHTName());
                                                Files.deleteIfExists(p);

                                            } else {
                                                LOGGER.severe("File " + dataItem.getName() + " could not be migrated to " + dispatchNode.getKey().toString());
                                            }
                                            fileResponseSocket.close();
                                            break;
                                        } catch (IOException e) {
                                            routingTable.deleteRoutingTableEntry(dispatchNode);
                                            dispatchNode = lookup(key);
                                        }
                                    }
                                }
                            }
                            LOGGER.info(routingTable.toString());
                            LOGGER.info(leafSet.toString());
                            LOGGER.info(displayFileList());
                        }
                        break;
                    case FILE_READ_REQUEST:
                        synchronized (sharedLock) {
                            FileRequest fileRequest = (FileRequest) request.getPayload();
                            fileRequest.incrementHopCount();
                            fileRequest.addToPath(nodeInformation.getKey());
                            LOGGER.info("File Read Request for " + fileRequest.getKey().toString() + ". Hop Count : " + fileRequest.getHopCount());

                            NodeInformation dispatchNode = lookup(fileRequest.getKey());

                            if (dispatchNode.equals(nodeInformation)) {
                                DataItem dataItem = null;

                                if (filesOnThisNode.containsKey(fileRequest.getKey())) {
                                    dataItem = filesOnThisNode.get(fileRequest.getKey());
                                }

                                Socket fileResponseSocket = new Socket(fileRequest.getNodeInformation().getNodeIP(), fileRequest.getNodeInformation().getNodePort());
                                ObjectOutputStream fileResponseSocketOOS = new ObjectOutputStream(fileResponseSocket.getOutputStream());

                                if (dataItem == null) {
                                    NodeInfoAndPath nodeInfoAndPath = new NodeInfoAndPath(nodeInformation, fileRequest.getPath());
                                    Packet fileResponse = new Packet(FILE_NOT_FOUND, nodeInfoAndPath);
                                    LOGGER.severe("File with identifier " + fileRequest.getKey().toString() + " not found.");
                                    fileResponseSocketOOS.writeObject(fileResponse);
                                } else {
                                    try {
                                        FileInputStream fis = new FileInputStream(dataItem.getDHTName());
                                        fis.close();
                                    } catch (FileNotFoundException f) {
                                        filesOnThisNode.remove(dataItem.getKey());
                                        LOGGER.severe("Could not find file " + dataItem.getDHTName());
                                    }
                                    long size = dataItem.getFileSize();
                                    long totalChunks = size / CHUNK_SIZE;

                                    if (size % CHUNK_SIZE != 0) {
                                        totalChunks++;
                                    }

                                    fileResponseSocketOOS.writeObject(new Packet(FILE_INFO, new FileAndPathInfo(dataItem, fileRequest.getPath())));

                                    long chunkID;
                                    boolean done = true;
                                    for (chunkID = 0; chunkID < totalChunks; chunkID++) {
                                        DataChunk dataChunk = new DataChunk(dataItem.getName(), size, chunkID);
                                        if (dataChunk.readDHT()) {
                                            fileResponseSocketOOS.writeObject(new Packet(FILE_CHUNK, dataChunk));
                                            //LOGGER.info("File chunk " + (chunkID+1) + " dispatched.");
                                        } else {
                                            fileResponseSocketOOS.writeObject(new Packet(FILE_CHUNK_WRITE_FAILED, null));
                                            done = false;
                                            LOGGER.severe("File read failed.");
                                        }

                                        fileResponseSocketOOS.flush();
                                        fileResponseSocketOOS.reset();
                                    }
                                    if (done) {
                                        ObjectInputStream fileResponseSocketOIS = new ObjectInputStream(fileResponseSocket.getInputStream());
                                        Packet ackPacket = (Packet) fileResponseSocketOIS.readObject();

                                        if (ackPacket.getType() == FILE_WRITE_SUCCESSFUL) {
                                            LOGGER.info("File " + dataItem.getName() + " successfuly sent to client.");
                                        } else {
                                            LOGGER.severe("File " + dataItem.getName() + " could not be sent to client.");
                                        }
                                    } else {
                                        LOGGER.severe("File " + dataItem.getName() + " could not be sent to client.");
                                    }
                                }
                                fileResponseSocket.close();
                            } else {
                                while (true) {
                                    try {
                                        Socket joinRequestSocket = new Socket(dispatchNode.getNodeIP(), dispatchNode.getNodePort());
                                        ObjectOutputStream joinRequestSocketOOS = new ObjectOutputStream(joinRequestSocket.getOutputStream());
                                        joinRequestSocketOOS.writeObject(new Packet(FILE_READ_REQUEST, fileRequest));
                                        LOGGER.info("File Read Request (" + fileRequest.getKey().toString() + ") dispatched to : " + dispatchNode.getKey().toString());
                                        joinRequestSocket.close();
                                        break;
                                    } catch (IOException e) {
                                        routingTable.deleteRoutingTableEntry(dispatchNode);
                                        dispatchNode = lookup(fileRequest.getKey());
                                    }
                                }
                            }
                        }
                        break;
                    case FILE_WRITE_REQUEST:
                        synchronized (sharedLock) {
                            FileRequest fileRequest = (FileRequest) request.getPayload();
                            fileRequest.incrementHopCount();
                            fileRequest.addToPath(nodeInformation.getKey());
                            LOGGER.info("File Write Request for " + fileRequest.getKey().toString() + ". Hop Count : " + fileRequest.getHopCount());

                            NodeInformation dispatchNode = lookup(fileRequest.getKey());

                            if (dispatchNode.getKey().equals(nodeInformation.getKey())) {
                                Socket fileResponseSocket = new Socket(fileRequest.getNodeInformation().getNodeIP(), fileRequest.getNodeInformation().getNodePort());
                                ObjectOutputStream fileResponseSocketOOS = new ObjectOutputStream(fileResponseSocket.getOutputStream());
                                FileWriteResponse fileWriteResponse = new FileWriteResponse(nodeInformation);
                                fileWriteResponse.initializeAndSetPath(fileRequest.getPath());
                                Packet fileResponse = new Packet(FILE_WRITE_RESPONSE, fileWriteResponse);
                                fileResponseSocketOOS.writeObject(fileResponse);
                                fileResponseSocket.close();
                            } else {
                                while (true) {
                                    try {
                                        Socket joinRequestSocket = new Socket(dispatchNode.getNodeIP(), dispatchNode.getNodePort());
                                        ObjectOutputStream joinRequestSocketOOS = new ObjectOutputStream(joinRequestSocket.getOutputStream());
                                        joinRequestSocketOOS.writeObject(new Packet(FILE_WRITE_REQUEST, fileRequest));
                                        LOGGER.info("File Write Request (" + fileRequest.getKey().toString() + ") dispatched to : " + dispatchNode.getKey().toString());
                                        joinRequestSocket.close();
                                        break;
                                    } catch (IOException e) {
                                        routingTable.deleteRoutingTableEntry(dispatchNode);
                                        dispatchNode = lookup(fileRequest.getKey());
                                    }
                                }
                            }
                        }
                        break;
                    case FILE_INFO:
                        synchronized (sharedLock) {
                            long chunkID;
                            DataItem dataItem = (DataItem) request.getPayload();
                            dataItem.setNodeInformation(nodeInformation);
                            long size = dataItem.getFileSize();
                            boolean done = true;
                            long totalChunks = size / CHUNK_SIZE;

                            if (size % CHUNK_SIZE != 0) {
                                totalChunks++;
                            }

                            Path p = Paths.get(dataItem.getDHTName());
                            Files.deleteIfExists(p);

                            for (chunkID = 0; chunkID < totalChunks; chunkID++) {
                                request = (Packet) clientSocketOIS.readObject();
                                if (request.getType() == FILE_CHUNK) {
                                    DataChunk dataChunk = (DataChunk) request.getPayload();
                                    if (dataChunk.writeDHT()) {
                                        //LOGGER.info("File chunk " + (chunkID+1) + " written to disk.");
                                    } else {
                                        done = false;
                                        LOGGER.info("File chunk " + (chunkID + 1) + " could not be written to disk.");
                                        break;
                                    }
                                } else {
                                    done = false;
                                    LOGGER.severe("File read failed.");
                                    break;
                                }
                            }

                            if (done) {
                                ObjectOutputStream clientSocketOOS = new ObjectOutputStream(clientSocket.getOutputStream());
                                clientSocketOOS.writeObject(new Packet(FILE_WRITE_SUCCESSFUL, null));
                                filesOnThisNode.put(dataItem.getKey(), dataItem);
                                LOGGER.info("File " + dataItem.getName() + " successfuly written to disk.");
                                LOGGER.info(displayFileList());
                            }
                        }
                        break;
                }
            } catch (IOException e) {
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            } catch (ReceivedMyOwnKeyException e) {
                e.printStackTrace();
            }
        }
    }

    private class UserInputThread extends Thread {
        @Override
        public void run() {
            Scanner inputScanner = new Scanner(System.in);
            while (true) {
                for (char c : inputScanner.next().toCharArray()) {
                    switch (c) {
                        case 'r':
                        case 'R': {
                                LOGGER.info(routingTable.toString());
                            }
                            break;
                        case 'l':
                        case 'L': {
                                LOGGER.info(leafSet.toString());
                            }
                            break;
                        case 'n':
                        case 'N': {
                                LOGGER.info(nodeInformation.toString());
                            }
                            break;
                        case 'q':
                        case 'Q':
                            synchronized (sharedLock) {
                                quitGracefully();
                            }
                            break;
                        case 'f':
                        case 'F': {
                                LOGGER.info(displayFileList());
                            }
                            break;
                        default: {
                                LOGGER.info("Invalid Input : " + c);
                            }
                    }
                }
            }
        }
    }
}
