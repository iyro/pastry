package cs555.pastry.packets;

import cs555.pastry.address.NodeInformation;
import cs555.pastry.hashing.Key;
import cs555.pastry.utilities.LoggingUtility;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.logging.Logger;

/**
 * Created by iyro on 10/5/15.
 */
public class RoutingUpdate implements Serializable {
    private static final Logger LOGGER = LoggingUtility.getLogger(RoutingUpdate.class.getName());
    private static final long serialVersionUID = -5365664949426635057L;
    NodeInformation nodeInformation;
    ArrayList<ArrayList<NodeInformation>> routingTableRows;
    NodeInformation[] leafSet;

    public RoutingUpdate(NodeInformation nodeInformation) {
        this.nodeInformation = nodeInformation;
        this.leafSet = null;
        this.routingTableRows = new ArrayList<>();
    }

    public NodeInformation getNodeInformation() {
        return nodeInformation;
    }

    public void addToRoutingTableRows(ArrayList<NodeInformation> row) {
        this.routingTableRows.add(row);
    }

    public ArrayList<ArrayList<NodeInformation>> getRoutingTableRows() {
        return routingTableRows;
    }

    public NodeInformation[] getLeafSet() {
        return leafSet;
    }

    public void setLeafSet(NodeInformation[] leafSet) {
        this.leafSet = leafSet;
    }

    public NodeInformation getLeftNeighbor() {
        return leafSet[0];
    }

    public NodeInformation getRightNeighbor() {
        return leafSet[1];
    }

    public Key getKey() {
        return nodeInformation.getKey();
    }
}
