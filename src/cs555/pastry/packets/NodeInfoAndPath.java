package cs555.pastry.packets;

import cs555.pastry.address.NodeInformation;
import cs555.pastry.hashing.Key;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by iyro on 10/14/15.
 */
public class NodeInfoAndPath implements Serializable {

    private static final long serialVersionUID = 7936656826101495009L;
    private NodeInformation nodeInformation;
    private ArrayList<Key> path;

    public NodeInfoAndPath(NodeInformation nodeInformation, ArrayList<Key> path) {
        this.nodeInformation = nodeInformation;
        this.path = path;
    }

    public NodeInformation getNodeInformation() {
        return nodeInformation;
    }

    public ArrayList<Key> getPath() {
        return path;
    }
}
