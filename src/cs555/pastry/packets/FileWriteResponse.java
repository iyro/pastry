package cs555.pastry.packets;

import cs555.pastry.address.NodeInformation;
import cs555.pastry.hashing.Key;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by iyro on 10/6/15.
 */
public class FileWriteResponse implements Serializable {
    private static final long serialVersionUID = -4885120743368903615L;
    NodeInformation nodeInformation;
    ArrayList<Key> path;

    public FileWriteResponse(NodeInformation nodeInformation) {
        this.nodeInformation = nodeInformation;
    }

    public NodeInformation getNodeInformation() {
        return nodeInformation;
    }

    public Key getKey() {
        return nodeInformation.getKey();
    }

    public void initializeAndSetPath(ArrayList<Key> path) {
        this.path = path;
    }

    public ArrayList<Key> getPath() {
        return path;
    }
}
