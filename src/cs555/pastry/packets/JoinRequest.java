package cs555.pastry.packets;

import cs555.pastry.address.NodeInformation;
import cs555.pastry.hashing.Key;
import cs555.pastry.utilities.LoggingUtility;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.logging.Logger;

/**
 * Created by iyro on 10/3/15.
 */
public class JoinRequest implements Serializable {
    private static final Logger LOGGER = LoggingUtility.getLogger(JoinRequest.class.getName());
    private static final long serialVersionUID = 5236991564445205594L;
    NodeInformation nodeInformation;
    int hopCount = 0;
    ArrayList<Key> path;

    public JoinRequest(NodeInformation newPeerNode) {
        this.nodeInformation = newPeerNode;
        this.hopCount = 0;
        this.path = new ArrayList<>();
    }

    public NodeInformation getNodeInformation() {
        return nodeInformation;
    }

    public int getHopCount() {
        return hopCount;
    }

    public void incrementHopCount() {
        this.hopCount++;
    }

    public Key getKey() {
        return nodeInformation.getKey();
    }

    public void addToPath(Key key) {
        path.add(key);
    }

    public ArrayList<Key> getPath() {
        return path;
    }
}

