package cs555.pastry.packets;

import cs555.pastry.address.NodeInformation;
import cs555.pastry.hashing.Key;
import cs555.pastry.items.DataItem;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by iyro on 10/13/15.
 */
public class FileAndPathInfo implements Serializable {
    private static final long serialVersionUID = 4111798137925094663L;
    DataItem dataItem;
    ArrayList<Key> path;

    public FileAndPathInfo(DataItem dataItem, ArrayList<Key> path) {
        this.dataItem = dataItem;
        this.path = path;
    }

    public DataItem getDataItem() {
        return dataItem;
    }

    public ArrayList<Key> getPath() {
        return path;
    }

    public Key getKey() {
        return dataItem.getKey();
    }

    public NodeInformation getNodeInformation() {
        return dataItem.getNodeInformation();
    }

    public void setNodeInformation(NodeInformation nodeInformation) {
        this.dataItem.setNodeInformation(nodeInformation);
    }

    public String getName() {
        return this.dataItem.getName();
    }

    public long getFileSize() {
        return this.dataItem.getFileSize();
    }

    public String getDHTName() {
        return this.dataItem.getDHTName();
    }
}
