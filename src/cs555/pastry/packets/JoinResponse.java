package cs555.pastry.packets;

import cs555.pastry.address.NodeInformation;
import cs555.pastry.hashing.Key;
import cs555.pastry.utilities.LoggingUtility;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.logging.Logger;

/**
 * Created by iyro on 10/3/15.
 */
public class JoinResponse implements Serializable {
    private static final Logger LOGGER = LoggingUtility.getLogger(JoinResponse.class.getName());
    private static final long serialVersionUID = 7974847602610916600L;
    NodeInformation nodeInformation;
    ArrayList<ArrayList<NodeInformation>> routingTableRows;
    NodeInformation[] leafSet;
    ArrayList<Key> path;

    public JoinResponse(NodeInformation nodeInformation) {
        this.routingTableRows = new ArrayList<>();
        this.leafSet = null;
        this.nodeInformation = nodeInformation;
    }

    public void initializeAndSetPath(ArrayList<Key> path) {
        this.path = path;
    }

    public NodeInformation getNodeInformation() {
        return nodeInformation;
    }

    public void addToRoutingTableRows(ArrayList<NodeInformation> row) {
        this.routingTableRows.add(row);
    }

    public ArrayList<ArrayList<NodeInformation>> getRoutingTableRows() {
        return routingTableRows;
    }

    public NodeInformation[] getLeafSet() {
        return leafSet;
    }

    public void setLeafSet(NodeInformation[] leafSet) {
        this.leafSet = leafSet;
    }

    public NodeInformation getLeftNeighbor() {
        return leafSet[0];
    }

    public NodeInformation getRightNeighbor() {
        return leafSet[1];
    }

    public Key getKey() {
        return nodeInformation.getKey();
    }

    public ArrayList<Key> getPath() {
        return path;
    }
}
