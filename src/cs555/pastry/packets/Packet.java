package cs555.pastry.packets;

import cs555.pastry.utilities.LoggingUtility;

import java.io.Serializable;
import java.util.logging.Logger;

/**
 * Created by iyro on 9/26/15.
 */
public class Packet implements Serializable {
    private static final Logger LOGGER = LoggingUtility.getLogger(Packet.class.getName());
    private static final long serialVersionUID = -8802395188359378722L;
    private packetType type;
    private Object payload;

    public Packet(packetType type, Object payload) {
        this.type = type;
        this.payload = payload;
    }

    public packetType getType() {
        return type;
    }

    public Object getPayload() {
        return payload;
    }

    public enum packetType {
        DISCOVER, DISCOVER_FILE, DISCOVER_SUCCESS, ADD_TO_DHT, DISCOVER_FAIL, JOIN_REQUEST, JOIN_RESPONSE, JOIN_RESPONSE_PROCESSED,
        ROUTING_UPDATE, FILE_READ_REQUEST, FILE_WRITE_REQUEST, FILE_INFO, FILE_WRITE_RESPONSE, FILE_CHUNK,
        FILE_NOT_FOUND, KILL_NODE, KILLED_NODE, KILL_FAILED, REMOVE_NODE, FILE_WRITE_FAILED, FILE_WRITE_SUCCESSFUL, FILE_CHUNK_WRITE_FAILED
    }

}
