package cs555.pastry.packets;

import cs555.pastry.address.NodeInformation;
import cs555.pastry.hashing.Key;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by iyro on 10/6/15.
 */
public class FileRequest implements Serializable {
    private static final long serialVersionUID = -5303360241119428859L;
    NodeInformation nodeInformation;
    int hopCount = 0;
    ArrayList<Key> path;

    public FileRequest(NodeInformation nodeInformation) {
        this.nodeInformation = nodeInformation;
        this.hopCount = 0;
        this.path = new ArrayList<>();
    }

    public NodeInformation getNodeInformation() {
        return nodeInformation;
    }

    public int getHopCount() {
        return hopCount;
    }

    public void incrementHopCount() {
        this.hopCount++;
    }

    public Key getKey() {
        return nodeInformation.getKey();
    }

    public void addToPath(Key key) {
        path.add(key);
    }

    public ArrayList<Key> getPath() {
        return path;
    }
}
