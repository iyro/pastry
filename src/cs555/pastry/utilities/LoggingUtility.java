package cs555.pastry.utilities;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.*;

/**
 * Created by iyro on 10/12/15.
 */
public class LoggingUtility {
    public static Logger getLogger(String className) {
        Logger logger = Logger.getLogger(className);

        logger.setUseParentHandlers(false);
        Handler conHdlr = new ConsoleHandler();
        conHdlr.setFormatter(new Formatter() {
            public String format(LogRecord record) {
                Date date = new Date(record.getMillis());
                SimpleDateFormat dt = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                return "[" + dt.format(date) + "] " + record.getLevel() + " : "
                        + record.getMessage() + "\n";
            }
        });
        logger.addHandler(conHdlr);

        return logger;
    }
}
