package cs555.pastry.utilities;

/**
 * Created by iyro on 9/25/15.
 */
public final class Constants {
    public static final int KEY_SIZE_IN_BYTES = 2;
    public static final int KEY_SIZE_IN_HEX_DIGITS = KEY_SIZE_IN_BYTES * 2;
    public static final int IDENTIFIER_SPACE_SIZE = 65536;
    public static final int LEAF_SET_SIZE = 2;
    public static final int POSSIBLE_DIGITS_IN_HEXADECIMAL = 16;
    public static final long CHUNK_SIZE = 64000;
}
