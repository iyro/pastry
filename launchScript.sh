#!/bin/bash

#wget http://www.cs.colostate.edu/~info/machines
#rm CSDEPT_HOSTS
#cat machines | grep general | grep "Linux(Fedora)" | grep "120-unix-lab" | awk '{print $1".cs.colostate.edu"}' | head -15 > CSDEPT_HOSTS
#rm machines

rm -rf out/
ant -f build/build.xml

cat CSDEPT_HOSTS | head -20 > PROCESS

scp out/artifacts/Pastry.jar frankfort.cs.colostate.edu:/s/chopin/a/grad/iyerro/

DISCOVERY=""
while IFS= read -r HOST;
do
    if [[ $DISCOVERY == "" ]];
    then
        DISCOVERY=$HOST
        gnome-terminal --name "$HOST" -e "ssh -t $DISCOVERY 'java -jar /s/chopin/a/grad/iyerro/Pastry.jar D'"
        continue
    else
        IFS= read -r KEY <&3;
        gnome-terminal --name "$HOST" -e "ssh -t $HOST 'java -jar /s/chopin/a/grad/iyerro/Pastry.jar P $KEY 0 $DISCOVERY 43210'"
    fi
    sleep 2
done <PROCESS 3<KEYS

rm PROCESS